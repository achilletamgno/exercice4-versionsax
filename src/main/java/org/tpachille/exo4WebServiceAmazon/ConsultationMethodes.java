package org.tpachille.exo4WebServiceAmazon;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class ConsultationMethodes extends DefaultHandler {


	/*Déclaration des variables*/
	int profondeur = 0,  
			compteur = 0,
	        compteur1 = 0,
			compteur2 = 0;
	String rootElemment=null,
			theItemShema = null,
			sousElement= null,

			nameSpace1=null,
			nameSpace2=null,

			prefixe=null,
			prefixe1=null,

			nameSpaceTmp=null,
			nameSpaceDefault=null;
	boolean iden = false;
	
	
	File xmfile=new File("files/AWSECommerceService.wsdl.xml");
	
	private static ArrayList<String> liste=new  ArrayList<String>();
	private static ArrayList<String> l=new  ArrayList<String>();
	static String ChainRetour=null;

	
	/*getteur sur chaine Retour*/
	public String getChainRetour() {
		return ChainRetour;
	}

	public void setChainRetour(String chainRetour) {
		ChainRetour = chainRetour;
	}


	
	/*Les starts*/
	public void startDocument()
	{
		System.out.println("ouverture de document \n");
	}
	
	public void startElement(String uri,String LocalName,String qName, Attributes attributes)
	{
		
		/*Elément racine*/
		if(rootElemment != null){
			rootElemment=qName;
		}

		/*attribut dans l'élément racine*/
		if(rootElemment == null){
			rootElemment=qName;

			for(int i=0; i<attributes.getLength(); i++){
				if(attributes.getLocalName(i) =="xmlns"){
					nameSpaceDefault = attributes.getValue(i);
					break;
				}
			}

			for(int i=0; i<attributes.getLength(); i++){
				if(attributes.getLocalName(i) =="xmlns:tns"){
					nameSpaceTmp = attributes.getValue(i);
					prefixe = attributes.getQName(i);
					break;
				}
			}

		}
		

		if(qName.equals("types"))
		{
			sousElement=qName;
		}
		if( sousElement!=null) 
		{
			if(qName.equals("xs:schema"))
			{
				iden=true;
			} 
			if(iden==true){

				if(profondeur==1)
				{
					compteur++;
				}
				profondeur++;
			}
		}

	}
	
	
	
    /*retourne une liste contenant les valeurs des attributs name des sous-éléments de Schema*/
	public static ArrayList<String> valeurAttributName() throws ParserConfigurationException, 
	                                                            SAXException, IOException
	{
		File xmfile=new File("files/AWSECommerceService.wsdl.xml");
		DefaultHandler handler1=new DefaultHandler()
		{
			String sousElement=null;
			boolean iden=false;
			int profondeur=0;


			public void startElement(String uri,String LocalName,String qName, Attributes attributes)
			{

				if(qName.equals("types"))
				{
					sousElement=qName;
				}
				if( sousElement!=null) 
				{
					if(qName.equals("xs:schema"))
					{
						iden=true;
					} 
				}

				if(iden==true)
				{
					profondeur++;
					if(profondeur==2)
						liste.add(attributes.getValue("name"));

				} 

			}
			public void endElement(String url,String localName,String qName)
			{
				if(qName.equals(sousElement))
				{
					sousElement=null;
				}

				if(qName.equals("xs:schema"))
					iden=false;

				if(iden==true)
					profondeur--;
			}
		};
		SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
		parser.parse(xmfile, handler1);
		return liste;
	}




	/* Methode retounant l'attribut name des éléments messages */
	public static ArrayList<String> valeurAttributNameMessage() throws ParserConfigurationException,
	                                                                   SAXException, IOException
	{

		File xmfile=new File("files/AWSECommerceService.wsdl.xml");
		DefaultHandler handler2=new DefaultHandler()
		{
			public void startElement(String uri,String LocalName,String qName, Attributes attributes)
			{
				if(qName.equals("message"))
				{
					l.add(attributes.getValue("name"));
				} 
			}
			public void endElement(String url,String localName,String qName)
			{	
				if(qName.equals("message")) {
				}
			}
		};
		SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
		parser.parse(xmfile, handler2);
		return l;
	}

   /* Methode retounant le Nom de l'opération utilisant le Message */
	public static String retournNomOperation( final String nomMessage) throws ParserConfigurationException,
	                                                                          SAXException, IOException
	{
		File xmfile=new File("files/AWSECommerceService.wsdl.xml");
		DefaultHandler handler3=new DefaultHandler()
		{
			String sousElement=null;
			boolean iden=false;
			int profondeur=0;

			public void startElement(String uri,String LocalName,String qName, Attributes attributes)
			{
				if(qName.equals("portType"))
				{
					sousElement=qName;
				}
				if( sousElement!=null) 
				{
					if(qName.equals("operation"))
					{
						iden=true;
					} 
				}

				if(iden==true)
				{
					profondeur++;
					if(profondeur==8)
					{
						if(nomMessage!=null)
							ChainRetour = attributes.getValue("name");
					}
				} 

			}
			public void endElement(String url,String localName,String qName)
			{
				if(qName.equals(sousElement))
				{
					sousElement=null;
				}

				if(qName.equals("operation"))
					iden=false;

				if(iden==true)
					profondeur--;
			}

		};
		SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
		parser.parse(xmfile, handler3);
		return ChainRetour;
	}


	/*les End*/
	public void endElement(String url,String localName,String qName)
	{
		rootElemment=qName;    // elt racine

		nameSpace1 = nameSpaceDefault;
		nameSpace2 = nameSpaceTmp; 

		prefixe1 = prefixe;

		//theItemShema = null; // arrivée à la fermeture de types
		compteur1 = compteur2;


		/*les sous élément de xs:shema*/
		if(qName.equals(sousElement))
		{
			for(int profond=profondeur; profond<sousElement.length(); profond++){
				theItemShema = sousElement ;
			}
			//theqNames = theItemShema
			//theItemShema=sousElement;
			sousElement=null;
		}
		

		if(qName.equals(sousElement))
		{
			sousElement=null;
		}
		profondeur--;	// décompte des élements comptés
		                // dans startElement();	
	}

	public void endDocument()
	{
		System.out.println("1. fichier Exo4TpXml");
		System.out.println("\n2a/ \nElement racine : "+rootElemment);
		System.out.println("\n2b/ \nEspace de nom par defaut : "+nameSpace1);
		System.out.println("\n3a/ \nEspace de nom propre à Amazon : "+nameSpace2);
		System.out.println("\n3b/ \nPréfixe : " + prefixe1);
		System.out.println("\n4a/ \n Nombre De sous-élément dans types/schema :"+ compteur);
	}


}
