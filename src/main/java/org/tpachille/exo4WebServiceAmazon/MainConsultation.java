package org.tpachille.exo4WebServiceAmazon;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class MainConsultation extends DefaultHandler  {


	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException{


		File xmlFile = new File("files/AWSECommerceService.wsdl.xml");

		ConsultationMethodes conslMeth = new ConsultationMethodes();		

		SAXParser parser = SAXParserFactory.newInstance().newSAXParser();

		parser.parse(xmlFile, conslMeth);

		System.out.println("4b/Liste des valeurs des attributs names shema sont : \n");
		for(String s : ConsultationMethodes.valeurAttributName())
		{
			System.out.println(s);
		}
		System.out.println("\n5/Liste des valeurs des attributs names message\n");
		for(String string : ConsultationMethodes.valeurAttributNameMessage())
		{
			System.out.println(string);
		}
		System.out.println("\n6/L'operation est : " + ConsultationMethodes.
				retournNomOperation("CartModifyResponseMsg"));

	}

}
